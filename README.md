# M03-Programació Intel·ligència Artificial

Resum genereal de la programació d'intel·ligència artificial

## Index  

1. [Heurística](Notebooks/Heur%C3%ADstica.ipynb) 
2. [NumPy](Notebooks/NumPy.ipynb)
3. [Matplotlib](Notebooks/Matplotlib.ipynb)
4. [Distribucions](Notebooks/Distribucions.ipynb)
5. [Models](Notebooks/Models.ipynb)

